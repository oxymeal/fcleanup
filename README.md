# fcleanup

Скрипт, который автоматически удаляет старые файлы и папки, в имени которых записана дата их создания.


## Установка

1. Скопируйте `fcleanup.py` в `$HOME/.local/bin`
2. Создайте в домашней папке файл `.fcleanup.json` со следующим содержимым:
   ```json
   {
       "dry": true,
       "entries": []
   }
   ```
3. Занесите `fcleanup.py` в крон (`crontab -e`). Например, чтобы он вызывался ежедневно:
    ```
    0 0 * * *       /home/<пользователь>/.local/bin/fcleanup.py
    ```

Готово, раз в день `fcleanup` будет удалять старые файлы согласно конфигурации в `.fcleanup.json`.


## Конфигурация

Чтобы проверить свою конфигурацию, вы можете вызвать `fcleanup.py` вручную.

```jsonc
{
    // Пока этот параметр задан на true, fcleanup не будет ничего не удалять,
    // но сообщит, что он собирался удалить. Это полезно для отладки конфигурации.
    // Исправьте на false, чтобы перевести fcleanup в рабочий режим.
    "dry": true,
    "entries": [
        {
            // Папка, в которой будут удаляться файлы
            "root_path": "/home/user/project/backups",
            // Формат имени файла, будет передан функции `datetime.strptime`.
            // Документацию по формату смотрите здесь: https://docs.python.org/3.8/library/datetime.html#strftime-and-strptime-format-codes
            "filename_template": "db%Y-%m-%d.sql",
            // Файлы, в имени которых даты старше 30 дней, будут удалены.
            "preserve_days": 30
        },
        // Еще один пример: часы-минуты-секунды тоже поддерживаются
        {
            "root_path": "/home/user/project/logs-archive",
            "filename_template": "backend_%Y-%m-%dT%H:%M:%S+00:00.log",
            "preserve_days": 90
        }
    ]
}
```
