#!/usr/bin/env python3
import json
import logging
import os
import shutil
from datetime import datetime, timedelta
from typing import List

HOME_PATH = os.path.expanduser('~')
CONFIG_PATH = os.path.join(HOME_PATH, '.fcleanup.json')

logging.basicConfig(
    format="[%(asctime)s %(levelname)s] %(message)s",
    level=logging.DEBUG,
)
logger = logging.getLogger(__name__)


class ConfigEntry:
    def __init__(
            self, root_path: str, filename_template: str, preserve_days: int):
        self.root_path = root_path
        self.filename_template = filename_template
        self.preserve_days = preserve_days

    def __repr__(self):
        return "ConfigEntry({})".format(self.__dict__)


class Config:
    def __init__(self, dry: bool, entries: List[ConfigEntry]):
        self.dry = dry
        self.entries = entries


def parse_config() -> Config:
    with open(CONFIG_PATH) as f:
        config_raw = json.load(f)

    dry = config_raw.get('dry', False)

    entries = []
    for entry_raw in config_raw['entries']:
        entries.append(ConfigEntry(
            root_path=entry_raw['root_path'],
            filename_template=entry_raw['filename_template'],
            preserve_days=entry_raw['preserve_days'],
        ))

    return Config(dry=dry, entries=entries)


def act_on_entry(config: Config, entry: ConfigEntry):
    logger.info("Working on entry %s", entry)

    filenames = os.listdir(entry.root_path)
    logger.info("%s contains %s files", entry.root_path, len(filenames))

    matching_names = []
    for name in filenames:
        try:
            dt = datetime.strptime(name, entry.filename_template)
        except ValueError:
            continue

        matching_names.append((name, dt))

    logger.info(
        "%s template matches %s files",
        entry.filename_template,
        len(matching_names),
    )

    names_to_delete = []
    now = datetime.now()
    for name, dt in matching_names:
        days_old = (now - dt) / timedelta(days=1)
        if days_old >= entry.preserve_days:
            logger.info(
                "%s is %s days old (more than %s), marking it for deletion",
                name, days_old, entry.preserve_days,
            )
            names_to_delete.append(name)

    logger.info("Prepared to delete %s files", len(names_to_delete))
    if not config.dry:
        for name in names_to_delete:
            path = os.path.join(entry.root_path, name)
            logger.info("Deleting %s", path)
            if os.path.isdir(path):
                shutil.rmtree(path)
            else:
                os.remove(path)


def main():
    config = parse_config()

    logger.info("Loaded %s config entries", len(config.entries))

    if config.dry:
        logger.info("Running in dry mode")

    for entry in config.entries:
        act_on_entry(config, entry)


if __name__ == "__main__":
    main()
